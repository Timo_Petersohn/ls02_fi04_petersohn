package kreis;

import java.awt.Point;

public class Quadrat {

	private double breite;
	private double laenge;
	private Point mittelpunkt;
	
	public Quadrat(double breite, double laenge, Point mittelpunkt)
	{
	  setMittelpunkt(mittelpunkt);
	  setBreite(breite);
	  setLaenge(laenge);
	}
	
	public void setBreite(double breite) {
		if(breite > 0)
			this.breite = breite;
		else
			this.breite = 0;
	}
	
	public void setLaenge(double laenge) {
		if(laenge > 0)
			this.laenge = laenge;
		else
			this.laenge = 0;
	}
	
	public double getDiagonale() {
		double diagonale;
		diagonale = Math.sqrt(Math.pow(this.breite, 2) * Math.pow(this.laenge, 2));
		return diagonale;
		
	}
	
	public double getFlaeche() {
        return this.breite * this.laenge;
    }
	
	public double getUmfang() {
		double umfang;
		umfang = breite*2 + laenge*2;
		return umfang;
	}
	
	public void setMittelpunkt(Point mittelpunkt) {
		this.mittelpunkt = mittelpunkt; 
	}
	
	public Point getMittelpunkt() {
		return mittelpunkt;
	}
}
