package kreis;

import java.awt.Point;

public class Testquadrat {

public static void main(String[] args) {
		
		Quadrat q = new Quadrat(50, 30, new Point(7,3));
        System.out.println("Diagonale: " + q.getDiagonale());
        System.out.println("Flaeche: " + q.getFlaeche());
        System.out.println("Umfang: " + q.getUmfang());
		
	}
}
