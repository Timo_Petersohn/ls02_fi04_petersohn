/**
 * Die Klasse Ladung stellt alle Parameter und Methoden zu Verfügung um Objekte der Klasse Ladung zu erstelllen und verwenden.
 * @author Timo Petersohn
 *@version 1.0
 */

public class Ladung {

	private String bezeichnung;
	private int menge;
	
/**
 * Konstruktor fuer die Ladung ohne parameterangabe. Hier wird auf Standardwerte zurueckgegriffen.
 */
	public Ladung() {
		this.bezeichnung = "Leer";
		this.menge = 0;
		
	}
	
/**
 * Konstruktor fuer die Ladung mit Parameterangabe	
 * @param bezeichnung
 * @param anzahl
 */
	public Ladung(String bezeichnung, int anzahl) {
		setBezeichnung(bezeichnung);
		setMenge(anzahl);
	}
	
/**
 * Die Bezeichnung der Ladung wird gesetzt/festgelegt.
 * @param bezeichnung
 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
/**
 * Die Menge der Ladung wird gesetzt/festgelegt
 * @param menge
 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
/**
 * Die Bezeichnung der Ladung wird weitgergegeben
 * @return this.bezeichnung
 */
	public String getBezeichnung(){
		return this.bezeichnung;
	}
/**
 * Die Menge der Ladung wird weitergegeben.
 * @return this.menge
 */
	public int getMenge(){
		return this.menge;
	}
}
