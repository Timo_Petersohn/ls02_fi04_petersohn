import java.util.ArrayList;
/**
 * @author Timo Petersohn.
 * @version 1.0.
 * Die Klasse Raumschiff enthält verschiedene Parameter und Methoden, welche zum konstruieren von Objekten der Klasse Raumschiff benötigt werden.
 * 
 */

public class Raumschiff {
//Zuerst die Parameter 

	private String schiffsName;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	
/**
 * parameterloser Konstruktor: Erstellt ein Objekt der Klasse Raumschiff ohne Parameter.
 * Es gibt keine Parameter und keinen Rückgabewert.
 * Array Listen hinzugefügt.
 */
	public Raumschiff() {
	//ohne Angaben der Parameter werden diese automatisch auf null gesetzt
	this.broadcastKommunikator = new ArrayList<String>();
	this.ladungsverzeichnis = new ArrayList<Ladung>();
	}

/**
 * Parametrisierter Konstruktor für ein Objekt der Klasse Raumschiff.
 * @param schiffsName = String zu Bezeichnung des Raumschiff Objekts.
 * 
 * alle folgenden Parameter sind integer.
 * 
 * @param photonentorpedoAnzahl.
 * @param energieversorgungInProzent.
 * @param schildeInProzent.
 * @param huelleInProzent.
 * @param lebenserhaltungssystemeInProzen.
 * @param androidenAnzahl.
 * Es gibt keinen wirklichen Rückgabewert, es wird ein Objekt der Klasse Raumschiff mit den genannten Parametern erstellt.
 */
	
	public Raumschiff(String schiffsName, int photonentorpedoAnzahl, 
			int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent,
			int androidenAnzahl) {
		this(); //ArrayListen aus default Konstruktor werden aufgerufen bzw. alle "this." Parameter etc.
		
		setSchiffsName(schiffsName);
		setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchildeInProzent(schildeInProzent);
		setHuelleInProzent(huelleInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAndroidenAnzahl(androidenAnzahl);
		

		
	}
	
/**
 * Der SchiffsName wird festgelegt.
 * @param schiffsName.
 */
	public void setSchiffsName(String schiffsName) {
	this.schiffsName = schiffsName;
	}
	
/**
 * Der SchiffsName wird gelesen/weitergegeben.
 * @return this.schiffsName.
 */
	public String getSchiffsName() {
		return this.schiffsName;
	}

/**
 * Die Anzahl der phtonenTorpedos wird gesetzt/festgelegt.
 * @param photonentorpedoAnzahl.
 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
/**
* Die Anzahl der photonentorpedos wird gelesen/weitergegeben.
* @return this.photonentopedoAnzahl.
*/
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
/**
 * Die Energieversorgung in Prozent wird gesetzt/festgelegt.
 * @param energieversorgungInProzent.
 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

/**
 * Die Energieversorgung in Prozent wird gelesen/weitergegeben.
 * @return this.energieversorgungInProzent.
 */
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
/**
 * Die Schilde in Prozent werden gesetzt/festgelegt.
 * @param schildeInProzent.
 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
/**
 * Die Schilde in Prozent werden gelesen/weitergegeben.
 * @return this.SchildeInProzent.
 */
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

/**
 * Die Huelle in Prozent wird gesetzt/festgelegt.
 * @param huelleInProzent.
 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
/**
 * Die Huelle in Prozent wird gelesen/weitergegeben.
 * @return this.huelleInProzent.
 */
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
/**
 * Die Lebenserhaltungssysteme in Prozent werden gesetzt/festgelegt.
 * @param lebenserhaltungssystemeInProzent.
 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

/**
 * Die Lebenserhaltungssysteme in Prozent werden gelesen/weitergegeben.
 * @return  this.lebenserhaltungssystemeInProzent.
 */
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
/**
 * Die androidenAnzahl wird gesetzt/festgelegt.
 * @param androidenAnzahl
 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

/**
 * Die androidenAnzahl wird gelesen/weitergegeben.
 * @return this.androidenAnzahl.
 */
	public int AndroidenAnzahl() {
		return this.androidenAnzahl;
	}

/**
 * Der BroadcastKommunikator wird gesetzt. 
 * Bei dieser Methode wird auf eine Array Liste gesetzt um alle Nachrichten zu speichern und gesammelt auszugeben.
 * @param broadcastKommunikator
 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	
/**
 * Der BroadcastKommunikator wird gelesen/weitergegeben.
 * @return this.broadcastKommunikator.
 */
	public ArrayList<String> getBroadcastKommunikator (){
		return this.broadcastKommunikator;
	}
	
/**
 * Der Ladungsverzeichnis wird gesetzt. 
 * Bei dieser Methode wird auf eine Array Liste gesetzt um alle Ladungen zu speichern und gesammelt auszugeben.
 * @param this.ladungsverzeichnis.
 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

/**
 * Das Ladungsverzeichnis wird gelesen/weitergegeben.
 * @return this.ladungsverzeichnis.
 */
	public ArrayList<Ladung> getLadungsverzeichnis(){
		return this.ladungsverzeichnis;
	}
	
//Methoden
/**
 * Es wird ein neuer Eintrag im Ladungsverzeichnis erstellt.
 * Außerdem wird ein entsprechendes neues Objekt der Klasse Ladung erstellt.
 * @param neueLadung
 */
	public void addLadung(Ladung neueLadung) {
		//neuer Eintrag in der ArrayList für die Ladung
		this.ladungsverzeichnis.add(neueLadung);
	}

/**
 * Es wird der Zustand des Schiffs ausgegeben.
 * Das bedeutet, alle Parameter werden mit ihren Werten ausgegeben.
 * Hierfür wird auf die Getter zurückgegriffen.
 */
	public void ausgabeZustand() {
		System.out.println(" - = * Schiff Zustand * = - ");
		System.out.println("Das schiff heißt " + this.schiffsName);
		System.out.println("Es sind " + photonentorpedoAnzahl + " Phtonentorpedos vorhanden");
		System.out.println("Die Energieversorgung liegt bei " + energieversorgungInProzent + "%");
		System.out.println("Die Schutzschilde sind bei " + schildeInProzent + "%");
		System.out.println("Die Huelle ist bei " + huelleInProzent + "%" );
		System.out.println("Die Lebenserhaltungssysteme sind bei " + lebenserhaltungssystemeInProzent + "%");
		System.out.println("Es sind noch " + androidenAnzahl + " Androiden vorhanden");
	}
	
/**
 * Es wird das Ladungsverzeichnis ausgegeben.
 * Hierfür wird mit einer Schleife jeder Eintrag in der ArrayList gelesen und ausgegeben. 
 */
	public void ausgabeLadungsverzeichnis() {
		for(Ladung l : this.ladungsverzeichnis) {
			System.out.println(" - = * Ladungsverzeichnis * = - ");
			System.out.println("Die Ladung ist " + l.getBezeichnung());
			System.out.println("Davon gibt es " + l.getMenge()+ " Stück");			
		}	
	}
	
/**
 * Es wird ein Photonentorpedo gescchossen.
 * Dabei wird zuerst geprüft, ob min. 1 PT vorhanden ist.
 * Wenn ja: wird eine Nachricht an alle gesendet und der Treffer vermerkt.
 * @param raumschiff
 */
	public void schiessePhotonentorpedos(Raumschiff raumschiff) {
		if(this.getPhotonentorpedoAnzahl() == 0) {
		this.nachrichtAnAlle("-=click=-");
		}else {
			this.photonentorpedoAnzahl--; //ein photonentorpedo weniger
			this.nachrichtAnAlle("Photonentorpedo wurde abgeschossen");
			raumschiff.vermerkeTreffer(raumschiff); 
		}
	}
	
/**
 * Es wird ein Schuss der Phaserkanone abgegeben.
 * Dabei wird zuerst geprüft, ob die Energieversorgung bei min. 50% liegt.
 * Wenn ja: wird eine Nachricht an alle gesendet und der Treffer vermerkt.
 * @param raumschiff
 */
	public void schiessePhaserkanonen(Raumschiff raumschiff) {
		if(this.getEnergieversorgungInProzent() < 50) {
			this.nachrichtAnAlle("-=*click*=-");
		}else {
			this.energieversorgungInProzent -= 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			this.vermerkeTreffer(raumschiff);
		}
	}
	
	
/**
 * Hier wird ein Treffer vermerkt.
 * Dabei wir geprüft ob:
 * 1. Die Schilde min. 51% haben; wenn ja werden 50% abgezogen
 * 		wenn nein: 2. Die Schilde werden auf 0%  gesetzt und 50% der Energieversorgung werden abgezogen.
 * 	UND wenn die Huelle kleiner als 51% ist, werden die Lebenserhaltungssysteme und die Huelle auf 0% gesetzt.
 * 			Außerdem wird in diesem Fall eine Nachricht an Alle ausgegeben.
 * Wenn die Huelle mehr als 50% hat, werden 50% abgezogen. 
 * @param raumschiff
 */
	public void vermerkeTreffer(Raumschiff raumschiff) {
		this.nachrichtAnAlle(raumschiff.getSchiffsName() + " wurde getroffen");//PRÜFEN
		if(raumschiff.getSchildeInProzent() > 51) {
			raumschiff.schildeInProzent -= 50;
		}else {
			raumschiff.schildeInProzent = 0;
			raumschiff.energieversorgungInProzent -= 50;
			if(raumschiff.getHuelleInProzent() < 51) {
				raumschiff.lebenserhaltungssystemeInProzent = 0;
				raumschiff.huelleInProzent = 0;
				this.nachrichtAnAlle("Die Lebenserhaltungssysteme von " + raumschiff.getSchiffsName() + " wurden vernichtet");
			}else {raumschiff.huelleInProzent -= 50;}
		}
	}
	
/**
 * Mit dieser Methode werden Einträge im BroadcastKommunikator erstellt.
 * Außerdem wird eine Konsolenausgabe mit eine textNachricht ausgegeben.
 * @param textNachricht
 */
	public void nachrichtAnAlle(String textNachricht) {
		this.broadcastKommunikator.add(textNachricht);
		System.out.println(textNachricht);
	}
	
/**
 * Mit dieser Methode werden alle Nachrichten/Eintraege des BroadcastKommunikators ausgegeben.
 * @param broadcastKommunikator
 */
	public void gibLogAus(ArrayList<String> broadcastKommunikator) {
		for(String b : broadcastKommunikator) {
		System.out.println(b);
		}
	}
	
	
}