
public class TestRaumschiff {

	public static void main(String[]args) {
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh`ta", 1, 100, 100, 100, 100, 2);
		klingonen.addLadung(new Ladung("Ferrengin Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung ("Rote Materie", 2));
		romulaner.addLadung(new Ladung ("Plasma-Waffe", 50));
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 100, 5);
		vulkanier.addLadung(new Ladung("Forschungssonden", 35));
		vulkanier.addLadung(new Ladung ("Photonentorpedo", 3));
	}
}
